var path = require('path');
var webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: './js/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.bundle.js'
    },
    module: {
        rules:[
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader'
            }
        ],        
    },
    optimization: {
        minimizer: [new UglifyJsPlugin()],
    },
    stats: {
        colors: true
    },
    devtool: 'source-map',
    plugins:[
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './index.html',
            title: 'Index',
            inject: false            
        }),
    ],
    devServer:{
        contentBase: path.join(__dirname, ""),
        port: 3000,
        publicPath: "http://localhost:3000/",
        hotOnly: true
    }
};