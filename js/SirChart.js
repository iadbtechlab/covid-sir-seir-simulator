import React from "react";
import { Line } from 'react-chartjs-2';
const rk4 = require("ode-rk4");


export function copy(x) {
    return Object.assign({},x)
}

export function simulate(f,t0,y0,step,tmax) {
    var integrator = rk4(y0, f, t0, step)
    var t = t0
    var y = y0
    var ta = []
    var ya = []
    ta.push(t0)
    ya.push(copy(y))
    while(true){
        t = t+step
        if(t>tmax) break
        integrator=integrator.step()
        ya.push(copy(integrator.y))
        ta.push(t)
    }
    return {t:ta,y:ya};
}

export const I0=1;
export const step=1;

class SirChart extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            data: null
        }
    }

    sirFn = (dydt, y, t)=>{
        dydt[0] = -(this.props.beta*y[0]*y[1])/this.props.population;
        dydt[1] = (this.props.beta*y[0]*y[1])/this.props.population - this.props.gamma*y[1];
        dydt[2] = this.props.gamma*y[1];
    }

    seirFn = (dydt, y, t)=>{
        dydt[0] = -(this.props.beta*y[0]*y[2])/this.props.population;
        dydt[1] = (this.props.beta*y[0]*y[2])/this.props.population - y[1]/this.props.incubation;
        dydt[2] = y[1]/this.props.incubation-this.props.gamma*y[2];
        dydt[3] = this.props.gamma*y[2];
    }

    updateData = () =>{
        let csvData = this.props.csvData;
        let confirmedColumn, deathsColumn, recoveredColumn;
        let confirmedData = [], deathsData = [], recoveredData = [], currentConfirmedData =[];
        if(Array.isArray(csvData)){
            for(var i=0;i<csvData[0].length;i++){
                if(csvData[0][i]==='date'){
                    confirmedColumn = i;
                }
                if(csvData[0][i]==='Confirmed'){
                    confirmedColumn = i;
                }
                if(csvData[0][i]==='Deaths'){
                    deathsColumn = i;
                }
                if(csvData[0][i]==='Recovered'){
                    recoveredColumn = i;
                }
            }
            
            for(var i=csvData.length-1;i>=0;i--){
                confirmedData.push(Number(csvData[i][confirmedColumn]));
                deathsData.push(Number(csvData[i][deathsColumn]));
                recoveredData.push(Number(csvData[i][recoveredColumn]));
                currentConfirmedData.push(
                    Number(csvData[i][confirmedColumn]) - Number(csvData[i][deathsColumn]) - Number(csvData[i][recoveredColumn])
                )
            }
        }

        if(this.props.sirSeir==0){
            let data = simulate(this.sirFn,0,[this.props.population-I0,I0,0.0], step, this.props.timeRange);
            let chartData = {
                labels: data.t,
                datasets:[
                    {
                        data: data.y.map((x)=>{return x[0]}),
                        label: 'Susceptible',
                        borderColor: 'rgb(205, 220, 57)',
                        backgroundColor: 'rgba(205, 220, 57, 0.5)'
                    },
                    {
                        data: data.y.map((x)=>{return x[1]}),
                        label: 'Infected',
                        borderColor: 'rgb(244, 67, 54)',
                        backgroundColor: 'rgba(244, 67, 54, 0.5)'
                    },
                    {
                        data: data.y.map((x)=>{return x[2]}),
                        label:'Recovered',
                        borderColor: 'rgb(3, 169, 244)',
                        backgroundColor: 'rgba(3, 169, 244, 0.5)'
                    }
                ]
            };
            if(currentConfirmedData.length>0){
                chartData['datasets'].push(
                    {
                        data: currentConfirmedData,
                        label: 'Real data Infected',
                        borderColor: 'rgb(233, 30, 99)',
                        backgroundColor: 'rgba(233, 30, 99, 0.5)'
                    }
                )
            }
            this.setState({data: chartData});
        }
        else if(this.props.sirSeir==1){
            let data = simulate(this.seirFn,0,[this.props.population-I0,0,I0,0.0], step, this.props.timeRange);
            let chartData = {
                labels: data.t,                
                datasets:[
                    {
                        data: data.y.map((x)=>{return x[0]}),
                        label: 'Susceptible',
                        borderColor: 'rgb(205, 220, 57)',
                        backgroundColor: 'rgba(205, 220, 57, 0.5)'
                    },
                    {
                        data: data.y.map((x)=>{return x[1]}),
                        label: 'Exposed',
                        borderColor: 'rgb(103, 58, 183)',
                        backgroundColor: 'rgba(103, 58, 183, 0.5)'
                    },
                    {
                        data: data.y.map((x)=>{return x[2]}),
                        label: 'Infected',
                        borderColor: 'rgb(244, 67, 54)',
                        backgroundColor: 'rgba(244, 67, 54, 0.5)'
                    },
                    {
                        data: data.y.map((x)=>{return x[3]}),
                        label: 'Recovered',
                        borderColor: 'rgb(3, 169, 244)',
                        backgroundColor: 'rgba(3, 169, 244, 0.5)'
                    },
                ]
            };
            if(currentConfirmedData.length>0){
                chartData['datasets'].push(
                    {
                        data: currentConfirmedData,
                        label: 'Real data Infected',
                        borderColor: 'rgb(233, 30, 99)',
                        backgroundColor: 'rgba(233, 30, 99, 0.5)'
                    }
                )
            }
            this.setState({data: chartData});
        }
    }

    componentDidMount(){
        this.updateData();
    }

    componentDidUpdate(prevProps){
        if (this.props.updateCount !== prevProps.updateCount) {
            this.updateData();
        }
        
    }

    render(){
        return(
            <div><Line data={this.state.data} width={600} height={300} options={this.props.options}></Line></div>
        )
    }
}

export default SirChart;
