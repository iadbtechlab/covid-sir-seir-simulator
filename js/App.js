import React from "react"
import SirChart from './SirChart';
import {parse} from 'papaparse'
import {nelderMead} from 'fmin'

class App extends React.Component{

    constructor(props){
        super(props);
        this.state = this.defaultState = {
            population: 10000, //people
            incubationTime: 14, //days,
            recoverTime: 14,
            betaCalculationMethodFile: false,
            gamma:1/14,
            nonCalculatedRo:5.7,
            calculatedRo: "",
            beta: 0.41,
            csvData: null,
            updateCount: 0,
            confirmedData: [],
            timeRange: 150,
            panelClass:'column-33',
            chartClass:'column-67'
        }
        this.fileRef = React.createRef();
        this.setState = this.setState.bind(this);
    }
    componentDidMount(){
         if(window.innerWidth<640){
             this.setState({
                 panelClass:'column-100',
                 chartClass: 'column-100'
             })
         }
    }

    nmCostFunction = (X)=>{
        let infectionProb = X[0];
        let nContact= 10;
        

        var res = this.state.confirmedData.map((value, index)=>{
            return  Math.exp( (infectionProb*nContact - this.state.gamma) * (index)) - value
        })
        var returnv = res.reduce((acc,curr)=>{
            return acc + Math.pow(curr,2)
        },0)
        return returnv;
    }

    calculateBetaFromFile =(confirmedData)=>{
        this.setState({confirmedData, confirmedData});
        var solution = nelderMead(this.nmCostFunction, [0.04]);
        var r0 = +10 * solution.x[0] / this.state.gamma;
        var beta = r0/this.state.recoverTime;
        this.setState({calculatedRo: r0, beta: beta});
    }

    onChangeData = (event)=>{
        this.setState({[event.target.name]:event.target.value});
        if(event.target.name=='recoverTime'){
            this.setState({
                gamma: Number(1/Number(event.target.value))
            })
        }
        if(!this.state.betaCalculationMethodFile && event.target.name=='nonCalculatedRo'){
            this.setState({
                beta: Number(event.target.value)/this.state.recoverTime
            });
        }
    }

    onChangeRo = (event)=>{
        if(!this.state.betaCalculationMethodFile){
            this.setState({
                nonCalculatedRo: event.target.value,
                beta: Number(event.target.value)/this.state.recoverTime
            });
        }
    }

    onBetaMethodChange =(event)=>{
        let beta;
        if(event.target.value=="calculated"){
            beta = "";
            this.resetFile();
        }
        else{
            beta = this.state.nonCalculatedRo/this.state.recoverTime;            
        }
        this.setState({
            betaCalculationMethodFile: event.target.value=="calculated"?true:false,
            beta: beta,
            calculatedRo: event.target.value=="calculated"?"":this.state.calculatedRo,
            csvData: null  
        })
    }

    resetFile=()=>{
        this.fileRef.current.value = null;
    }

    onChangeFile = (event)=>{
        var setState = this.setState;
        var calculateBetaFromFile = this.calculateBetaFromFile;
        var resetFile = this.resetFile;
        if(event.target.files.length>0){
            parse(event.target.files[0], {
                complete: (results, file)=>{
                    if(results.data.length>0){
                        let confirmedColumn = null;
                        setState({csvData: results.data});
                        for(var i=0;i<results.data[0].length;i++){
                            if(results.data[0][i]==='Confirmed'){
                                confirmedColumn = i;
                            }
                        }
                        if(confirmedColumn){
                            let confirmedData = []
                            for(var i=1;i<results.data.length;i++){
                                confirmedData.push(Number(results.data[i][confirmedColumn]));
                            }
                            //setState({confirmedData: confirmedData});
                            calculateBetaFromFile(confirmedData);
                        }
                        else{
                            alert("CSV must include column 'Confirmed'");
                            resetFile();
                        }
                    }
                },
                error: (error)=>{
                    console.log(error);
                    alert("CSV does not have a valid format.")
                    resetFile();
                }
            })
        }
        
    }

    updateCharts = () =>{
        this.setState({
            updateCount: this.state.updateCount+1
        })
    }

    render(){
        return(
            <div className="container">
                <br/>
                <div className="row">
                    
                    <h2>SIR / SEIR Dashboard</h2>                                       
                </div>
                <div className="row">
                    <p>
                        This dashboard simulates how illness virus spreading in a group and illustrate why social distance is important 
                        for containment using SIR (Susceptible, Infected and Recovered) and SEIR (Susceptible, Exposed, Infected and Recovered) models.
                    </p>                    
                </div>
                <div className="row">
                    <p>
                        For more information please read the article on <a href="https://idbg.sharepoint.com/sites/tech_lab#/ProjectId/26">"Mathematical modeling on epidemics prediction"</a> 
                        and visit the <a href="https://bitbucket.org/iadbtechlab/covid-sir-seir-simulator/">Bitbucket repository</a> for all the programming code used in building this dashboard.
                    </p>
                </div>
                <div>
                <h3>Calculate the models in 5 steps.</h3>
                </div>
                <div className="row" >
                    <div className={"column box box-even "+this.state.panelClass}>
                        <br/>
                        <div className="">
                            <p>1. Define population</p>
                            <fieldset>
                                <label htmlFor="sirBeta">Population</label>
                                <input type="number" id="population" name="population" value={this.state.population} onChange={this.onChangeData}  required/>                         
                            </fieldset>
                        </div>
                        <div className="">
                            <p>2. Calculate Beta from R0</p>
                            <div>
                                <div>
                                    <input type="radio" name="betaMethod" id="betaMethod1" checked={this.state.betaCalculationMethodFile?false:true} value="preset" onChange={this.onBetaMethodChange}/>
                                    <label htmlFor="betaMethod1" style={{display:'inline', marginLeft:'5px'}} >
                                        Preset R0
                                    </label>
                                    <span style={{margin:'0 5px'}}></span>
                                    <input type="radio" name="betaMethod" id="betaMethod2" checked={this.state.betaCalculationMethodFile?true:false} value="calculated" onChange={this.onBetaMethodChange}/>
                                    <label htmlFor="betaMethod2" style={{display:'inline', marginLeft:'5px'}}>Calculate R0 from file</label>
                                </div>
                                <div hidden={this.state.betaCalculationMethodFile?true:false}>
                                    <div>
                                        <label htmlFor="r0">R0</label>
                                        <input type="number" id="nonCalculatedRo" name="nonCalculatedRo" value={this.state.nonCalculatedRo} onChange={this.onChangeRo}  required/>
                                    </div>
                                    <p>
                                        <label>Beta</label>
                                        {this.state.beta && this.state.beta!==""?this.state.beta.toFixed(2):"-"} 
                                    </p>
                                    {/* <div>
                                        <label htmlFor="beta">Beta</label>
                                        <input type="number" id="beta1" name="beta" value={this.state.beta} required readOnly={true} disabled/>
                                    </div> */}
                                </div>
                                <div hidden={this.state.betaCalculationMethodFile?false:true}>
                                    <div>
                                        <label htmlFor="r0">File</label>
                                        <input type="file" onChange={this.onChangeFile} accept=".csv" ref={this.fileRef}/>
                                    </div>
                                    <div>
                                        <label htmlFor="r0">R0</label>
                                        <input type="number" id="calculatedRo" name="calculatedRo" value={this.state.calculatedRo} readOnly disabled/>
                                    </div>
                                    <p>
                                        <label>Beta</label>
                                        {this.state.beta && this.state.beta!==""?this.state.beta.toFixed(2):"-"} 
                                    </p>
                                    {/* <div>
                                        <label htmlFor="beta">Beta</label>
                                        <input type="number" id="beta2" name="beta" value={this.state.beta} required readOnly={true} disabled/>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                        <div>
                            <p>3. Set recovery time</p>
                            <div>
                                <label htmlFor="seirTe">Recovery time (days)</label>
                                <input type="number" id="recoverTime" name="recoverTime" value={this.state.recoverTime} onChange={this.onChangeData} required/>
                                <p>
                                    <label>Gamma (1/recovery time)</label>
                                    {this.state.gamma.toFixed(2)} 
                                </p>
                                {/* <label htmlFor="sirGamma">Gamma (1/recovery time)</label>
                                <input type="number" id="gamma" name="gamma" value={this.state.gamma} required readOnly={true} disabled/>                                 */}
                            </div>
                        </div>
                        <div>
                            <p>4. Set incubation time (flattening the curve)</p>
                            <div>
                                <label htmlFor="seirTe">Incubation Time (days)</label>
                                <input type="number" id="incubationTime" name="incubationTime" value={this.state.incubationTime} onChange={this.onChangeData} required/>
                            </div>
                        </div>
                        <div>
                            <p>5. Refresh charts</p>
                            <button className="button button-accent" id="buttonUpdateSeir" onClick={this.updateCharts}>Refresh Charts</button>
                        </div>
                        <br/>
                    </div>
                    <div className={"column column-xs-100 "+this.state.chartClass}>
                        <div style={{width: '100%'}}>
                            <SirChart 
                                sirSeir="0" 
                                population={this.state.population} 
                                beta={this.state.beta} 
                                gamma={this.state.gamma} 
                                updateCount={this.state.updateCount} 
                                timeRange={this.state.timeRange}
                                options={
                                    {
                                        title:{
                                            display: true,
                                            text: "SIR Model"
                                        }
                                    }
                                }
                                csvData={this.state.csvData}
                            >                                    
                            </SirChart>
                            <SirChart 
                                sirSeir="1" 
                                population={this.state.population} 
                                beta={this.state.beta} 
                                gamma={this.state.gamma} 
                                incubation={this.state.incubationTime} 
                                updateCount={this.state.updateCount}
                                timeRange={this.state.timeRange}
                                options={
                                    {
                                        title:{
                                            display: true,
                                            text: "SEIR Model"
                                        }
                                    }
                                }
                                csvData={this.state.csvData}
                            >                                
                            </SirChart>
                            {/* <canvas id="sir-chart" width="500px" height="300px"></canvas> */}
                        </div>
                        <div style={{padding:'1em'}}>
                            <span style={{marginRight: '15px'}}>Time Range:</span>
                            <input type="range" name="timeRange" min="90" max="180" value={this.state.timeRange} onChange={this.onChangeData}/>
                            <span style={{marginLeft: '15px'}}>{this.state.timeRange} days</span>
                        </div>
                    </div>                    
                </div>
                <div className="row">
                    <br/>
                    Built by IDB's TechLab
                    <br/>
                    <br/>
                </div>
            </div> 
        )
    }
}

export default App;