# SIR / SEIR Dashboard

SIR/SEIR Dashboard is a React JS project that simulates how illness virus spreading in a group and illustrate why social distance is important for containment using SIR (Susceptible, Infected and Recovered) and SEIR (Susceptible, Exposed, Infected and Recovered) models.

Work is based on IDB's TechLab article on ["Mathematical modeling on epidemics prediction"](https://idbg.sharepoint.com/sites/tech_lab#/ProjectId/26).

## Requirements
Node JS

## Installation

Clone the repository and install the packages.

```bash
npm install
```

## Usage

### Run in localhost
```bash
npm run start:dev
```
Open http://localhost:3000/ in any web browser.

### Build
```bash
npm run build
```
Build files are located within the dist/ directory.

### Using the Dashboard

Dashboard guides the user to simulate models in 5 steps:

1. Define population. Requires user to specify an integer number. Default value: 10000
2. Calculate Beta from R0. For this provides two methods:
    - Using a preset R0 value. Default: 5.7
    - From File. This option requires the user to upload a CSV file with the following columns: "Date", "Confirmed", "Recovered", "Deaths". Once uploaded it calculates R0 and then beta. You can find the file format in the data folder
    of this repository, filename: Sample_COVID.csv
3. Set recovery time. Default: 14 days.
4. Set incubation time. This variable is used in SEIR models and regulates the exposure of population and therefore can be used for flattening the curve.
5. Click Refresh Charts button.

## Acknowledgements

This project uses the following libraries:

- [Chart JS](https://www.chartjs.org/)
- [ODE-RK4](https://github.com/scijs/ode-rk4)
- [fmin](https://github.com/benfred/fmin)
- [Papaparse](https://www.papaparse.com/)

### Python Notebooks

Simulations are also available as Python's notebook. Access using the following links.


## License
[MIT](https://choosealicense.com/licenses/mit/)